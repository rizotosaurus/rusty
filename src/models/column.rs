use yew::prelude::*;
use yew::{function_component, props, Properties};


#[derive(Clone, Properties, PartialEq)]
pub struct ChessColumnProps {
    pub id: String,
    pub piece: Piece,
}

#[derive(Clone, PartialEq)]
pub enum Piece {
    Rook,
    Knight,
    Bishop,
    King,
    Queen,
    NoPiece
}

fn turnToSquare(props: &ChessColumnProps, number: i32) -> String {
    format!("{}{}", props.id.clone(), number)
}

#[function_component(ChessColumn)]
pub fn chess_column(props: &ChessColumnProps) -> Html {
    html! {
        <section class={classes!(props.id.clone())}>
            <article id={turnToSquare(props, 8)}><img draggable="true" src="b_rook.png" /></article>
            <article id={turnToSquare(props, 7)}><img draggable="true" src="b_pawn.png"/></article>
            <article id={turnToSquare(props, 6)}></article>
            <article id={turnToSquare(props, 5)}></article>
            <article id={turnToSquare(props, 4)}></article>
            <article id={turnToSquare(props, 3)}></article>
            <article id={turnToSquare(props, 2)}><img draggable="true" src="w_pawn.png"/></article>
            <article id={turnToSquare(props, 1)}><img draggable="true" src="w_rook.png"/></article>
        </section>
    }
}
