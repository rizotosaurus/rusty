mod models;

use yew::prelude::*;
use models::column::ChessColumn;
use models::column::Piece;

pub struct Board {}
pub enum Msg {}

impl Component for Board {
    type Message = Msg;
    type Properties = ();


    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        false
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <main class={classes!("board")}>
                <ChessColumn piece={Piece::Rook}    id={"A".to_string()} />
                <ChessColumn piece={Piece::Knight}  id={"B".to_string()} />
                <ChessColumn piece={Piece::Bishop}  id={"C".to_string()} />
                <ChessColumn piece={Piece::Queen}   id={"D".to_string()} />
                <ChessColumn piece={Piece::King}    id={"E".to_string()} />
                <ChessColumn piece={Piece::Bishop}  id={"F".to_string()} />
                <ChessColumn piece={Piece::Knight}  id={"G".to_string()} />
                <ChessColumn piece={Piece::Rook}    id={"H".to_string()} />
            </main>
        }
    }
}


fn main() {
    yew::start_app::<Board>();
}
